--Q1 create table personne


  create table personne_glinot (
  p VARCHAR(4) primary key ,
  nom varchar(15) NOT NULL,
  prenom varchar(15) NOT NULL,
  sexe char(1) CHECK (sexe='M'or sexe='F'));




--Q2 fill database with file

\COPY personne_glinot01 FROM 'X:/BD-2annee/TP3/personne.txt' WITH DELIMITER ',' -- pas de ;

--Q3 create table film

CREATE TABLE film_glinot (
  f VARCHAR(4) primary key,
  titre VARCHAR(25),
  annee INTEGER,
  p_rea VARCHAR(4) references personne_glinot, -- film_glinot.p_rea inclu dans personne_glinot.p
  genre VARCHAR(15),
  duree INTEGER CHECK (duree > 0)


);


--Q4 add line
INSERT INTO film_glinot VALUES('f1','LANCELOT',1995,'p9','LEGENDES',133);
--Q5 check table content
SELECT * FROM film_glinot;
--Q6 fill table film
\COPY film_glinot FROM 'X:/BD-2annee/TP3/film.txt' WITH DELIMITER ','

--Q7 create table acteur

CREATE TABLE acteur_glinot (
  p VARCHAR(4) primary key REFERENCES personne_glinot,
  nb_film INTEGER
);

-- fill table
\COPY acteur_glinot FROM 'X:/BD-2annee/TP3/acteur.txt' WITH DELIMITER ','
-- create table vedette
CREATE TABLE vedette_glinot(
  f VARCHAR(4) REFERENCES film_glinot ,
  p VARCHAR(4) REFERENCES personne_glinot,
role varchar(25) ,
primary key(f,p)
);


-- fill table
\COPY vedette_glinot FROM 'X:/BD-2annee/TP3/vedette.txt' WITH DELIMITER ','
-- check grants
\z film_glinot;
\z personne_glinot;
\z acteur_glinot;
\z vedette_glinot;
GRANT SELECT ON film_glinot,personne_glinot,acteur_glinot,vedette_glinot TO PUBLIC ;
GRANT SELECT,UPDATE,INSERT ON film_glinot,personne_glinot,acteur_glinot,vedette_glinot TO cvuaille ;
GRANT ALL ON  film_glinot,personne_glinot,acteur_glinot,vedette_glinot TO cvuaille;



-- Q11
SELECT DISTINCT titre, annee , personne_glinot.nom ,genre , duree
FROM film_glinot,personne_glinot
WHERE film_glinot.p_rea=personne_glinot.p;


--Q12
SELECT DISTINCT P.nom, P.prenom , F.titre
FROM personne_glinot AS P , film_glinot AS F, vedette_glinot AS V
WHERE P.p =V.p AND V.f=F.f AND F.titre LIKE '%MANHATTAN%';

--Q13
SELECT P.nom,P.prenom
FROM acteur_glinot AS A , personne_glinot as P
WHERE A.p = P.p AND  A.nb_film > (SELECT AVG(nb_film)
FROM acteur_glinot);
--Q14 tills lösung !!! 
SELECT personne_glinot.nom
FROM personne_glinot P , film_glinot A ,film_glinot B
WHERE P.p =A.p_real AND A.f <> B.f AND A.p_real = B.p_real;
--Q15
SELECT personne_glinot.nom
FROM (SELECT COUNT(p_rea) AS count,p_rea AS p_rea FROM film_glinot GROUP BY p_rea) AS CREA , personne_glinot
WHERE CREA.p_rea = personne_glinot.p and CREA.count >3;

--Q16
CREATE VIEW filmpial_glinot
AS SELECT f
FROM  film_glinot,personne_glinot
 WHERE film_glinot.p_rea = personne_glinot.p AND personne_glinot.nom = "LALLALA";
SELECT personne_glinot.nom , personne_glinot.pernom
FROM personne_glinot
WHERE p IN
(SELECT p
FROM personnes_glinot,vedette_glinot,film_glinot
WHERE vedette_glinot.f=film_glinot.f AND personne_glinot.p = vedette_glinot.p AND personne_glinot.nom="lalla"
GROUP BY personne_glinot.p
HAVING COUNT(*)=(SELECT
 COUNT(*) FROM filmpial_glinot));
 DROP VIEW filmpial_glinot;
